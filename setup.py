from setuptools import setup

setup(
    name='cards',
    packages=['cards'],
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    install_requires=[
        'invoke',
        'flask',
        'pyaml',
        'logging_tree',
    ],
    entry_points={
        'console_scripts': ['cards = cards.main:program.run']
    },
    include_package_data=True,
    package_data={
        "cards": ["*.yaml"]
    }
)


