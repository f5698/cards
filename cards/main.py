from invoke import Program, Collection
import cards.french_deck

program = Program(namespace=Collection.from_module(cards.french_deck), version='0.1.0')
